###################################################################
###################################################################
###################################################################
######                                                       ######
######          Backup Email Notification System             ######
######               Written by Ben Simpson                  ######
######                                                       ######
###################################################################
###################################################################
###################################################################

require 'mailman'
require 'net/smtp'
require 'rufus-scheduler'

#######################
# Email configuration #
#######################

Mailman.config.pop3 = {
  server: 'pop.server.com', 
  port: 110,
  ssl: false,
  username: "backupemailreceiver@example.com",
  password: "password"
}
Mailman.config.poll_interval = 60
Mailman::Application.run do

  ###########
  # Clients #
  ###########

  clients = { 
    "clientemail@example.com" => false,
    "anotherclient@example.com" => false
  }

  ############
  # Schedule #
  ############

  Rufus::Scheduler.new.every '1d' do
    failed_clients = []
    missing_clients = []

    clients.each do |client, received|
      if received == "failed"
        failed_clients << client
      elsif !received
        missing_clients << client
      end
    end

    if failed_clients.any? || missing_clients.any?
      missing_clients = missing_clients.reduce("") { |list, client| list << "#{client}\n" }
      failed_clients = failed_clients.reduce("") { |list, client| list << "#{client}\n" }

      ###################
      # Failure Message #
      ###################

      message = <<MESSAGE_END
From: Backup Alerts <backup-alerts@example.com>
To: My Name <myself@example.com>
Subject: Backup Failure

#{ "Missing backups for:" unless missing_clients.empty? }
#{ missing_clients }

#{ "Backups failed for:" unless failed_clients.empty? }
#{ failed_clients }
MESSAGE_END
    else

      ###################
      # Success message #
      ###################

      message = <<MESSAGE_END
From: Backup Alerts <backup-alerts@example.com>
To: My Name <myself@example.com>
Subject: Backup Success

All backups completed successfully

MESSAGE_END
    end

    ##############
    # Send Email #
    ##############

    Net::SMTP.start(
      'smtp.server.com',                # smtp server
      25,                               # port
      'example.com',                    # domain
      'backupemailreceiver@example.com',# email address
      'password',                       # password
      :plain                            # authentication
      ) do |smtp|
      smtp.send_message message, 'backup-alerts@example.com', 'myself@example.com' # from, to
    end
    clients.each { |_, status| status = false }
  end

  ###############
  # Email Rules #
  ###############

  # Failure if body of email includes "Red"
  from('%client%').body("Red") do
    clients[params[:client]] = "failed" if clients[params[:client]] #remove if to automatically add new clients
  end

  # Failure if body of email includes "unsuccessful"
  from('%client%').body("unsuccessful") do
    clients[params[:client]] = "failed" if clients[params[:client]] #remove if to automatically add new clients
  end

  # Success if previous conditions were false
  from '%client%' do
    clients[params[:client]] = true if clients[params[:client]] #remove if to automatically add new clients
  end
end