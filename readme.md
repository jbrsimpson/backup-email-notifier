Backup Email Notification System

This program will monitor an email address for backup emails. If it doesn't receive an email from a particular client, or if the email indicates that a backup failed, it will send an email to you letting you know.


Required Gems:

mailman

rufus-scheduler